A la découverte d'un Système d'Exploitation - Vendredi 11 septembre 2020 
********************************

1. ![Movie](images\bureau.png)

2.  Modifier 3 paramètres de configuration de la barre des taches et expliquer ce qu’ils changent :  

        -Afficher les contacts, petite icône à côté du drive.
        -changement de la barre des tâches en haut, à droite ou à gauche.
        -masquer les étiquettes.

3. Qu'est ce que la résolution?

        La définition d’écran, couramment appelée à tort résolution d'écran, est le nombre de points ou pixels que peut afficher un écran. La définition est le produit du nombre de points selon l’horizontale par le nombre de points selon la verticale de l’affichage.

4. Comment la mesure-t-on ?

        Pour calculer la définition d'une image numérique, il suffit de multiplier le nombre de pixels sur la hauteur par le nombre de pixels sur la largeur de l'image. Par exemple, une image de 6000 x 4000 px a une définition de 24 millions de pixels, ou 24 mégapixels

5. Déterminer le nombre de pixels correspondant ?

        Cette image a une taille corespondante à 845x473.

6. Qu’est ce qu’un pixel ?

        Chaque point d'une image électronique.

7. Naviguer dans le menu et tester 3 paramètres d’affichage, expliquer ce qu’ils changent :

        Résolution de l'écran, il formate l'écran parmis 3 types, on peut choisir laquelle nous convient le mieux.
        Modifier la taille du texte. (+/-)
        Orientation affichage (paysage).


8. Naviguer dans le menu et tester 3 paramètres de son, expliquer ce qu’ils changent :

        Régler le volume de différents sons.
        Changer le modèle de son.
        Brancher des périphériques.

9. Naviguer dans le menu et tester 3 paramètres d’alimentation et mise en veille, expliquer ce
qu’ils changent :

        Définir quand l'ordinateur doit se mettre en veille.
        Economiser l'énergie et prolonger l'autonomie de la batterie.

10. Naviguer dans le menu et tester 3 paramètres de stockage, expliquer ce qu’ils changent :

        Gérer les espaces de stockage.
        Optimiser les lecteurs.
        Afficher l'utilisation du stockage sur les autres lecteurs.

11. Naviguer dans le menu et tester 3 paramètres de protection, expliquer ce qu’ils changent :

        Protection du compte
        Protectin du réseau
        Protection des pare-feu

12.  Dans les informations système notez :
        - Le nom de l’appareil :  INFOST-1-15
        - Le processeur : Intel(R) Core(TM) i7-4770 CPU @3,40GHz...

13.  Quel est le nombre de coeur du processeur ? 

                Il y a 4 coeurs.

14.  Qu’est ce que cela veut dire ?

                Un processeur multi-cœur permet à l'utilisateur d'exécuter plusieurs tâches en même temps sans subir de ralentissements.

15. Quelle est la fréquence du processeur ?

                3,40GHz

16. Quelle est l’unité de mesure de la mémoire RAM ?

               Go (giga octet)        

17. Quelle est la quantité de mémoire RAM ?

                Sur ce PC elle est de 32Go.

18. Convertissez cette quantité en Mega-octet :

                32768 Mo

19. Convertissez cette quantité en Octet :

                34359738368 octets

20. Convertissez cette quantité en Bit :

                274877906944 bits

21. Quel est le type du système d’exploitation ?

                Microsoft Windows 10 Education N

22. Qu’est ce que cela veut dire ?

                

